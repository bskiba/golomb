#!/bin/bash

# Script that finds the freshest output file and plots the results (fitness by step)

LAST_BEST=`ls -1 results | grep '_best' | tr "\n" " " | awk '{print $NF}'`
LAST_AVG=`ls -1 results | grep '_avg' | tr "\n" " " | awk '{print $NF}'`
LAST_CFG=`ls -1 results | grep '_config' | tr "\n" " " | awk '{print $NF}'`

# Create a new dir for the plot
COUNTER=1
while [[ -d "plots/plot_last_${COUNTER}" ]]
do
	COUNTER=$((COUNTER + 1))
done
DIR_NAME="plots/plot_last_${COUNTER}"
mkdir $DIR_NAME

# Copy the files
cp results/${LAST_BEST} ${DIR_NAME}/
cp results/${LAST_AVG} ${DIR_NAME}/
cp results/${LAST_CFG} ${DIR_NAME}/config.cfg

# Format config file
sed -i "s/export //g" ${DIR_NAME}/config.cfg
sed -i "s/VERBOSE.*//g" ${DIR_NAME}/config.cfg
sed -i "s/#.*//g" ${DIR_NAME}/config.cfg
sed -i "/./,/^$/!d" ${DIR_NAME}/config.cfg 	# Remove blank lines from beggining 


if [[ -z "$LAST_BEST" ]]; then
	echo "Nothing to plot!"
	exit 1
fi

LAST_BEST_CONTENT_L=`cat results/$LAST_BEST | wc -l`
if [[ $LAST_BEST_CONTENT_L -lt 1 ]]; then
	echo "Last best fitness file is empty"
	exit 1
fi

AVG_BEST_CONTENT_L=`cat results/$LAST_AVG | wc -l`
if [[ $AVG_BEST_CONTENT_L -lt 1 ]]; then
	echo "Last avg fitness file is empty"
	exit 1
fi


TITLE=$(head -n 1 results/${LAST_BEST})
TITLE_PROPER=`echo $TITLE | sed 's/^..//'`

echo "plotting: "
echo $LAST_BEST
echo $LAST_AVG

gnuplot -p -e "filename_best='results/${LAST_BEST}'" \
	-e "filename_avg='results/${LAST_AVG}'"  -e "plot_title='${TITLE_PROPER}'" \
	-e "plot_output='${DIR_NAME}/plot.png'" gnuplot_config.cfg