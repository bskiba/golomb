#!/bin/bash

# Script that accumulates results from all output files placed in given directory and plots the averaged result (fitness by step)

if [[ ! -d "$1" ]]; then
    echo "Specify directory containing fitness files to plot."
    exit 1
fi


# Create a new dir for the plot
COUNTER=1
while [[ -d "plots/plot_summary_${COUNTER}" ]]
do
    COUNTER=$((COUNTER + 1))
done
DIR_NAME="plots/plot_summary_${COUNTER}"
mkdir $DIR_NAME

# Copy the files
cp $1/*_best.txt ${DIR_NAME}/
cp $1/*_avg.txt  ${DIR_NAME}/

# Copy last config file - we assume all simulations had the same config
CFG_FILES=`ls -1 $1 | grep '_config' | tr "\n" " "`
LAST_CFG=`echo $CFG_FILES | awk '{print $NF}'`

cp $1/$LAST_CFG ${DIR_NAME}/config.cfg

# Format config file
sed -i "s/export //g" ${DIR_NAME}/config.cfg
sed -i "s/VERBOSE.*//g" ${DIR_NAME}/config.cfg
sed -i "s/#.*//g" ${DIR_NAME}/config.cfg
sed -i "/./,/^$/!d" ${DIR_NAME}/config.cfg  # Remove blank lines from beggining 


BEST_FILES=`ls -1 $1 | grep '_best' | tr "\n" " "`
LAST_BEST=`echo $BEST_FILES | awk '{print $NF}'`
LINES=`cat $1/$LAST_BEST | wc -l`

NUM_FILES=`echo $BEST_FILES | wc -w`

# Calc summary data for plots, it will be output in
# best_summary.txt and avg_summary.txt fiels in the same dir
python summarize.py ${DIR_NAME}

# Calculate how often should error bars be plotted
# Try to achieve about 500 error bars per chart
if [[ $LINES -lt 251 ]]; then
    LINES=251
fi
ERROR_BARS_EVERY=$(((LINES + 249) / 500))

TITLE=$(head -n 1 ${DIR_NAME}/${LAST_BEST})
TITLE_PROPER=`echo $TITLE | sed 's/^..//'`

gnuplot -p -e "filename_best='${DIR_NAME}/summary_best.txt'" \
    -e "filename_avg='${DIR_NAME}/summary_avg.txt'"  -e "plot_title='${TITLE_PROPER} - summary of ${NUM_FILES} runs.'" \
    -e "best_plot_output='${DIR_NAME}/plot_best.png'" -e "avg_plot_output='${DIR_NAME}/plot_avg.png'" \
    -e "error_bars_every=${ERROR_BARS_EVERY}" gnuplot_summary_config.cfg