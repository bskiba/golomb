#!/bin/bash

if [[ ! -f "$1" ]]; then
    echo "Please provide path to config file in the first argument."
    exit 0
fi

source $1

export CONFIG_FILE=$1

python -m golomb.bootstrap golomb.golombconf