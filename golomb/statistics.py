import logging
import time
import sys
import shutil
from pyage.core.statistics import Statistics

logger = logging.getLogger(__name__)

# Zbiera statystyki z symulacji. Pozwala to
# - okreslic progres symulacji
# - zbierac wiedze o rozkladzie fitnessu w genotypach na poszczegolnych wyspach (potrzebne do mutacji COMMA)
# - zapisywac wyniki do pliku
class CommaStatistics(Statistics):
    # Ta zmienna statyczna pozwoli nam znac max fitness globalnie
    max_fitness = -99999999999999999

    def __init__(self, step_limit, config_file, header='LABS COMMA', output_file_name='fitness_pyage', verbose=False):
        self.history = []
        self.fitness_output = open('results/' + output_file_name + '_best.txt', 'a')
        self.avg_fitness_output = open('results/' + output_file_name + '_avg.txt', 'a')
        shutil.copyfile(config_file, 'results/' + output_file_name + '_config.txt')
        self.fitness_output.write("# %s\n" % header)
        self.avg_fitness_output.write("# [AVERAGE] %s\n" % header)
        self.step_limit = step_limit
        self.verbose = verbose
        # Draw progress bar
        print "0% ========================================== 100%"

    def __del__(self):
        self.fitness_output.close()
        self.avg_fitness_output.close()

    def append(self, step_count, best_fitness, avg_fitness):
        self.fitness_output.write(str(step_count - 1) + ' ' + str(best_fitness) + '\n')
        self.avg_fitness_output.write(str(step_count - 1) + ' ' + str(avg_fitness) + '\n')

    def update(self, step_count, agents):
        try:
            if self.verbose >= 1 and step_count % max(1, int(self.step_limit / 100)) == 0 :
                print "___________________________________________  %3d%%" % (step_count * 100 / self.step_limit)
                print "island | agents | max fitness | min fitness"

            else:                
                self.draw_progress(step_count, self.step_limit),

            for idx, a in enumerate(agents):
                agents_on_island = a._AggregateAgent__agents.values()
                # Pusta wyspa
                if len(agents_on_island) == 0:
                    max_on_island = -100000
                else:
                    max_on_island = agents_on_island[0].get_fitness();   
                
                min_on_island = max_on_island
               
                for agent in agents_on_island:
                    fitness = agent.get_fitness()
                    min_on_island = min(fitness, min_on_island)
                    max_on_island = max(fitness, max_on_island)
                for agent in agents_on_island:
                    agent.genotype.max_fitness = max_on_island
                    agent.genotype.min_fitness = min_on_island

                if self.verbose and step_count % max(1, int(self.step_limit / 100)) == 0 :
                    print "%6d | %6d | %11.3f | %11.3f" % (idx, len(agents_on_island), max_on_island, min_on_island)
            
            if self.verbose >= 2 and step_count % max(1, int(self.step_limit / 100)) == 0 :
                print "__________________________________________________"
                for i, a in enumerate(agents):
                    print "island %d" % i
                    print_island(a)
                print ">%3d%%" % (step_count * 100 / self.step_limit)

            best_fitness = max(a.get_fitness() for a in agents)
            avg_fitness = avg(agents)
            logger.info(best_fitness)
            self.history.append(best_fitness)
            self.append(step_count, best_fitness, avg_fitness)

            # Set globally known max fitness, if it improved
            if (best_fitness > CommaStatistics.max_fitness):
                CommaStatistics.max_fitness = best_fitness

        except:
            logging.exception("")
            print sys.exc_info()[0]

    def summarize(self, agents):
        print
        try:
            logger.debug(self.history)
            logger.debug("best genotype: %s", max(agents, key=lambda a: a.get_fitness()).get_best_genotype())
            print "best genotype: %s" % (max(agents, key=lambda a: a.get_fitness())).get_best_genotype()
        except:
            logging.exception("")

    # Draw progress on the progress bar
    def draw_progress(self, current_step, all_steps):
        if (current_step % max(1, int(all_steps / 50)) == 0):
            sys.stdout.write("^")
            sys.stdout.flush()


def avg(agents):   
    sum = 0
    total_size = 0
    for a in agents:
        agents_on_island = a._AggregateAgent__agents.values()
        total_size += len(agents_on_island)
        for agent in agents_on_island:
            sum += agent.get_fitness()
    return sum * 1. / total_size

def print_island(island):
    agents_on_island = island._AggregateAgent__agents.values()
    print len(agents_on_island)
    for a in agents_on_island:
        print '%s, e:%s' % (str(a.genotype), str(a.get_energy()))
    print "total energy: %d" % (sum(a.get_energy() for a in agents_on_island),)