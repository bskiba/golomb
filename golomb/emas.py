import logging
import random

from pyage.core.address import Addressable
from pyage.core.agent.aggregate import get_random_move
from pyage.core.inject import Inject, InjectWithDefault

try:
    from golomb.evaluation import GolombSimpleEvaluation
except:
    try:
        from evaluation import GolombSimpleEvaluation
    except:
        raise

logger = logging.getLogger(__name__)


class GolombAgent(Addressable):
    @Inject("migration", "evaluation", "crossover", "mutation", "emas", "transferred_energy",
            "max_same_fitness_transferred_energy", "min_same_fitness_transferred_energy", "fitness_sharing")
    def __init__(self, genotype, energy, name=None):
        self.name = name
        super(GolombAgent, self).__init__()
        self.genotype = genotype
        self.energy = energy
        self.steps = 0
        self.dead = False
        self.evaluation.process([genotype])

    def step(self):
        self.steps += 1
        if self.dead:
            return
        try:
            neighbour = self.parent.get_neighbour(self)
            if neighbour:
                if self.emas.should_die(self):
                    self.death()
                elif self.emas.should_reproduce(self, neighbour):
                    self.emas.reproduce(self, neighbour)
                else:
                    self.meet(neighbour)
            if self.emas.can_migrate(self):
                self.migration.migrate(self)
            elif self.parent and self.emas.should_move(self):
                self.parent.move(self)
        except:
            logging.exception('')

    def get_fitness(self):
        return self.genotype.fitness

    def get_best_genotype(self):
        return self.genotype

    def add_energy(self, energy):
        self.energy += energy
        if self.emas.should_die(self):
            self.death()

    def get_energy(self):
        return self.energy

    def get_genotype(self):
        return self.genotype

    def meet(self, neighbour):
        logger.debug(str(self) + "meets" + str(neighbour))
        if self.get_fitness() > neighbour.get_fitness():
            transferred_energy = min(self.transferred_energy, neighbour.energy)
            self.energy += transferred_energy
            neighbour.add_energy(-transferred_energy)
        elif self.get_fitness() < neighbour.get_fitness():
            transferred_energy = min(self.transferred_energy, self.energy)
            self.energy -= transferred_energy
            neighbour.add_energy(transferred_energy)
        # Tutaj nastepuje wymiana energii, jesli fitness jest taki sam.
        # Ilosc wymienionej energi zalezy liniowo od podobienstwa dwoch genotypow
        # Jesli sa one identyczne, wymienione zostanie <max_same_fitness_transferred_energy> energii (parametr klasy)
        # Jesli maja zerowe podobienstwo, <min_same_fitness_transferred_energy> energii (parametr klasy)
        # TODO - zerowe podobienstwo to tak naprawde calkowite podobienstwo, poniewaz odwrocenie wszystkich bitow 
        # nie wplywa w ogole na merit factor. Mozna to tez uwzglednic.
        else:  # same fitness
            if self.fitness_sharing:
                transfered_energy = self.max_same_fitness_transferred_energy - \
                                    (
                                    self.max_same_fitness_transferred_energy - self.min_same_fitness_transferred_energy) * \
                                    self.hamming_distance(self.get_genotype().direct(),
                                                          neighbour.get_genotype().direct()) / len(
                                        self.get_genotype().direct())
                transfered_energy = int(transfered_energy)
                transfered_energy = min(transfered_energy, self.energy)
                neighbour.add_energy(transfered_energy)
                self.add_energy(-transfered_energy)

        if self.emas.should_die(self):
            self.death()

    def death(self):
        self.distribute_energy()
        self.energy = 0
        self.dead = True
        self.parent.remove_agent(self)
        logger.debug(str(self) + "died!")

    def distribute_energy(self):
        logger.debug("%s is dying, energy level: %d" % (self, self.energy))
        if self.energy > 0:
            siblings = set(self.parent.get_agents())
            siblings.remove(self)
            portion = self.energy / len(siblings)
            if portion > 0:
                logger.debug("passing %d portion of energy to %d agents" % (portion, len(siblings)))
                for agent in siblings:
                    agent.add_energy(portion)
            left = self.energy % len(siblings)
            logger.debug("distributing %d left energy" % left)
            while left > 0:
                e = min(left, 1)
                siblings.pop().add_energy(e)
                left -= e

    def hamming_distance(self, genes_a, genes_b):
        distance = 0
        for x in xrange(0, len(genes_a)):
            if genes_a[x] != genes_b[x]:
                distance = distance + 1
        return distance

    def __repr__(self):
        return "<EmasAgent@%s>" % self.get_address()


class GolombEmasService(object):
    @Inject("minimal_energy", "reproduction_minimum", "migration_minimum", "newborn_energy",
            "memetic_mutation", "memetic_mutation_tree_width", "memetic_mutation_tree_height")
    @InjectWithDefault(("move_probability", 0.1))
    def __init__(self):
        super(GolombEmasService, self).__init__()

    def should_die(self, agent):
        return agent.get_energy() <= self.minimal_energy and not agent.dead

    def should_reproduce(self, a1, a2):
        return a1.get_energy() > self.reproduction_minimum and a2.get_energy() > self.reproduction_minimum \
               and a1.parent.locator.get_allowed_moves(a1)

    def can_migrate(self, agent):
        return agent.get_energy() > self.migration_minimum and len(agent.parent.get_agents()) > 10

    def should_move(self, agent):
        return random.random() < self.move_probability

    def reproduce(self, a1, a2):
        logger.debug(str(a1) + " " + str(a2) + " reproducing!")
        energy = self.newborn_energy / 2 * 2
        a1.energy -= self.newborn_energy / 2
        a2.add_energy(-self.newborn_energy / 2)
        genotype = a1.crossover.cross(a1.genotype, a2.get_genotype())
        genotype = self.mutate(a1, genotype)
        newborn = GolombAgent(genotype, energy)
        a1.parent.locator.add_agent(newborn, get_random_move(a1.parent.locator.get_allowed_moves(a1)))
        a1.parent.add_agent(newborn)

    def mutate(self, a1, original_genotype):
        if self.memetic_mutation:
            return self.mutate_memetic(a1, original_genotype, self.memetic_mutation_tree_width, self.memetic_mutation_tree_height)
        else:
            a1.mutation.mutate(original_genotype)
            return original_genotype


    def mutate_memetic(self, a1, genotype, tree_width, tree_height_left):
        best_genotype = genotype
        best_fitness = -99999999999
        for i in xrange(tree_width):
            new_genotype = genotype.copy()
            a1.mutation.mutate(new_genotype)
            new_fitness = GolombSimpleEvaluation().errorRateAndLength(new_genotype)
            if new_fitness > best_fitness:
                best_genotype = new_genotype
                best_fitness = new_fitness
        if tree_height_left == 1:
            return best_genotype
        else:
            return self.mutate_memetic(a1, best_genotype, tree_width, tree_height_left - 1)



