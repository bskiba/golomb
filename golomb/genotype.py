from copy import deepcopy

# genes is a list of markers
class GolombGenotype(object):
    def __init__(self, representation, min_fitness=0, max_fitness=0, direct = True):
        super(GolombGenotype, self).__init__()
        self.fitness = None
        # Parametry potrzebne przy COMMA
        self.min_fitness = min_fitness
        self.max_fitness = max_fitness
        
        #tworzymy z reprezentacji bezposredniej
        if direct:
            self.set_direct(representation)
        #tworzymy z reprezentacji posredniej
        else:
            self.set_indirect(representation)


    def __str__(self):
        return "%s, f:%s" % (self.__direct, self.fitness)

    def __repr__(self):
        return self.__str__()

    def __compute_indirect(self, direct):
        indirect = []
        for i in xrange(len(direct) - 1):
            indirect.append(direct[i + 1] - direct[i])
        return indirect

    def __compute_direct(self, indirect):
        direct = [0]
        for length in indirect:
            direct.append(direct[-1] + length)
        return direct

    def set_direct(self, direct):
        self.__direct = direct
        self.__indirect = self.__compute_indirect(direct)

    def set_indirect(self, indirect):
        self.__indirect = indirect
        self.__direct = self.__compute_direct(indirect)

    def direct(self):
        return self.__direct

    def indirect(self):
        return self.__indirect

    #dlugosc linijki to polozenie ostatniego markera
    def length(self):
        return self.__direct[-1]

    def copy(self):
        return deepcopy(self)