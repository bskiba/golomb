# coding=utf-8
import logging
import os
import datetime

from pyage.core import address
from pyage.core.agent.agent import unnamed_agents
from pyage.core.agent.aggregate import AggregateAgent
from pyage.core.emas import EmasService
from pyage.core.locator import TorusLocator
from pyage.core.locator import RandomLocator
from pyage.core.migration import ParentMigration
from pyage.core.stats.gnuplot import StepStatistics
from pyage.core.stop_condition import StepLimitStopCondition
from pyage.solutions.evolution.crossover import SinglePointCrossover
from pyage.solutions.evolution.evaluation import FloatRastriginEvaluation
from pyage.solutions.evolution.initializer import float_emas_initializer
from pyage.solutions.evolution.mutation import UniformFloatMutation


try:
    from golomb.genotype import GolombGenotype
except:
    try:
        from genotype import GolombGenotype
    except:
        raise

try:
    from golomb.mutation import MoveMarkerMutation
except:
    try:
        from mutation import MoveMarkerMutation
    except:
        raise

try:
    from golomb.mutation import MoveAllMarkersMutation
except:
    try:
        from mutation import MoveAllMarkersMutation
    except:
        raise

try:
    from golomb.mutation import MoveAllOrShuffleMutation
except:
    try:
        from mutation import MoveAllOrShuffleMutation
    except:
        raise

try:
    from golomb.mutation import CommaMoveMarkerMutation
except:
    try:
        from mutation import CommaMoveMarkerMutation
    except:
        raise

try:
    from golomb.mutation import CommaMoveAllMarkersMutation
except:
    try:
        from mutation import CommaMoveAllMarkersMutation
    except:
        raise

try:
    from golomb.mutation import CommaMoveAllOrShuffleMutation
except:
    try:
        from mutation import CommaMoveAllOrShuffleMutation
    except:
        raise

try:
    from golomb.crossover import GolombSinglePointCrossover
except:
    try:
        from crossover import GolombSinglePointCrossover
    except:
        raise

try:
    from golomb.crossover import GolombUniformCrossover
except:
    try:
        from crossover import GolombUniformCrossover
    except:
        raise

try:
    from golomb.crossover import GolombUniformCrossover
except:
    try:
        from crossover import GolombUniformCrossover
    except:
        raise

try:
    from golomb.evaluation import GolombSimpleEvaluation
except:
    try:
        from evaluation import GolombSimpleEvaluation
    except:
        raise

try:
    from golomb.initializer import golomb_emas_initializer
except:
    try:
        from initializer import golomb_emas_initializer
    except:
        raise

try:
    from golomb.emas import FitnessSharingEmasService
except:
    try:
        from emas import GolombEmasService
    except:
        raise

try:
    from golomb.statistics import CommaStatistics
except:
    try:
        from  statistics import CommaStatistics
    except:
        raise

step_limit = int(os.environ['STEP_LIMIT'])
dims = int(os.environ['GENOTYPE_LENGTH'])
island_count = int(os.environ['ISLANDS'])
size = int(os.environ['AGENTS_PER_ISLAND'])

starting_energy = int(os.environ['STARTING_ENERGY'])
minimal_energy = lambda: int(os.environ['MINIMAL_ENERGY'])
reproduction_minimum = lambda: int(os.environ['REPRODUCTION_MINIMUM'])
migration_minimum = lambda: int(os.environ['MIGRATION_MINIMUM'])
newborn_energy = lambda: int(os.environ['NEWBORN_ENERGY'])
transferred_energy = lambda: int(os.environ['TRANSFERRED_ENERGY'])

fitness_sharing = lambda: os.environ['FITNESS_SHARING'] == 'true'
min_same_fitness_transferred_energy = \
    lambda: int(os.environ['MIN_SAME_FITNESS_TRANSFERRED_ENERGY'])
max_same_fitness_transferred_energy = \
    lambda: int(os.environ['MAX_SAME_FITNESS_TRANSFERRED_ENERGY'])

crossover_type = os.environ['CROSSOVER_TYPE']
if (crossover_type == 'uniform'):
    max_parent_influence = int(os.environ['UNIFORM_MAX_PARENT_INFLUENCE'])

mutation_type = os.environ['MUTATION_TYPE']
if (mutation_type == 'comma_move_marker'):
    comma_min_strength = int(os.environ['COMMA_MIN_STRENGTH'])
    comma_max_strength = int(os.environ['COMMA_MAX_STRENGTH'])
    comma_bad_mutation_prob = float(os.environ['COMMA_BAD_MUTATION_PROBABILITY'])
if (mutation_type == 'move_all_markers') or (mutation_type == 'move_or_shuffle') \
    or mutation_type == 'comma_move_all_markers' or mutation_type == 'comma_move_or_shuffle':
    all_markers_mut_max_str = int(os.environ['ALL_MARKERS_MUTATION_MAX_STRENGTH'])

memetic_mutation = lambda: os.environ['MEMETIC_MUTATION'] == 'true'
memetic_mutation_tree_width = \
    lambda: int(os.environ['MEMETIC_MUTATION_TREE_WIDTH'])
memetic_mutation_tree_height = \
    lambda: int(os.environ['MEMETIC_MUTATION_TREE_HEIGHT'])

verbose = int(os.environ['VERBOSE'])
config_file = os.environ['CONFIG_FILE']

logger = logging.getLogger(__name__)

agents = unnamed_agents(island_count, AggregateAgent)

stop_condition = lambda: StepLimitStopCondition(step_limit)

aggregated_agents = lambda: golomb_emas_initializer(dims, energy=starting_energy, size=size)
emas = GolombEmasService

evaluation_type = os.environ['EVALUATION']
evaluation_factor = int(os.environ['EVALUATION_FACTOR'])

if(evaluation_type == 'golomb_simple'):
    golomb_simple = GolombSimpleEvaluation(evaluation_factor)
    evaluation = lambda: golomb_simple
else:
    raise Exception('Invalid evaluation type')

if (crossover_type == 'single_point'):
    crossover = lambda: GolombSinglePointCrossover()
elif (crossover_type == 'uniform'):
    crossover = lambda: GolombUniformCrossover(max_parent_influence = max_parent_influence)
else:
    raise Exception('Invalid crossover type!')

if (mutation_type == 'move_marker'):
    mutation = lambda: MoveMarkerMutation()
    simulation_type = "simple EMAS"
elif (mutation_type == 'comma_move_marker'):
    mutation = lambda: CommaMoveMarkerMutation(bad_mutation_probability=comma_bad_mutation_prob,
                                               min_strength=comma_min_strength, max_strength=comma_max_strength)
    simulation_type = "EMAS with COMMA"
elif (mutation_type == 'move_all_markers'):
    mutation = lambda: MoveAllMarkersMutation(max_strength=all_markers_mut_max_str)
    simulation_type = "simple EMAS"
elif (mutation_type == 'move_or_shuffle'):
    mutation = lambda: MoveAllOrShuffleMutation(max_strength=all_markers_mut_max_str)
    simulation_type = "simple EMAS"
elif (mutation_type == 'comma_move_all_markers'):
    mutation = lambda: CommaMoveAllMarkersMutation(max_strength=all_markers_mut_max_str)
    simulation_type = "EMAS with COMMA"
elif (mutation_type == 'comma_move_or_shuffle'):
    mutation = lambda: CommaMoveAllOrShuffleMutation(max_strength=all_markers_mut_max_str)
    simulation_type = "simple EMAS"
else:
    raise Exception('Invalid mutation type!')

address_provider = address.SequenceAddressProvider

migration = ParentMigration

torus = TorusLocator(10, 10)
locator = lambda: torus

stats = lambda: CommaStatistics(
    step_limit=step_limit,
    config_file=config_file,
    header='Golomb Ruler, %s genotype length %d' % (simulation_type, dims),
    output_file_name='fitness_%s' % str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")),
    verbose=verbose)
