from pyage.core.operator import Operator

# Wylicza fitness genotypu z uwzglednieniem ilosci bledow w linijce
# oraz dlugosci linijki
class GolombSimpleEvaluation(Operator):

    def __init__(self, factor = 250):
        super(GolombSimpleEvaluation, self).__init__()
        self.factor = factor

    def process(self, population):
        for genotype in population:
            genotype.fitness = self.errorRateAndLength(genotype)
            
    def errorRateAndLength(self, genotype):
        errors = 0
        distances = set()
        #wyliczenie ilosci powtorzen
        for (i, marker) in enumerate(genotype.direct()):
            for other_marker in genotype.direct()[i + 1:]:
                distance = other_marker - marker
                if distance in distances:
                    errors += 1
                else:
                    distances.add(distance)

        # chcemy minimalizowac wiec dajemy ujemny fitness 
        return -(self.factor * errors + genotype.length())