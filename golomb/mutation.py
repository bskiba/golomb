import random
import math
from copy import deepcopy

from pyage.solutions.evolution.mutation import AbstractMutation

try:
    from golomb.genotype import GolombGenotype
except:
    try:
        from genotype import GolombGenotype
    except:
        raise

try:
    from golomb.evaluation import GolombSimpleEvaluation
except:
    try:
        from evaluation import GolombSimpleEvaluation
    except:
        raise

try:
    from golomb.statistics import CommaStatistics
except:
    try:
        from statistics import CommaStatistics
    except:
        raise


# Przesuwa jeden z markerow w przestrzeni pomiedzy sasiednimi markerami
class MoveMarkerMutation(AbstractMutation):
    def __init__(self):
        # 1.0 - probability, potrzebne tylko do konstruktora
        super(MoveMarkerMutation, self).__init__(GolombGenotype, 1.0)

    def mutate(self, genotype):
        move_marker(genotype)


# MoveMarkerMutation wykonywane kilka razy (tym wiecej, im slabszy genotyp)
class CommaMoveMarkerMutation(AbstractMutation):
    def __init__(self, bad_mutation_probability=0.2, min_strength=1, max_strength=3):
        super(CommaMoveMarkerMutation, self).__init__(GolombGenotype,
                                                      1.0)  # 1.0 - probability, potrzebne tylko do konstruktora
        self.bad_mutation_probability = bad_mutation_probability  # szansa, ze gorszy genotyp zastapi lepszy
        self.min_strength = min_strength
        self.max_strength = max_strength


    def mutate(self, genotype):
        # Fitness aktualnego rozwiazania
        fitness = GolombSimpleEvaluation().errorRateAndLength(genotype)
        # Kopia genow
        new_genotype = genotype.copy()

        for i in xrange(get_mutation_power(fitness, genotype.min_fitness, genotype.max_fitness, self.min_strength,
                                           self.max_strength)):
            move_marker(new_genotype)

        # Fitness nowego rozwiazania
        new_fitness = GolombSimpleEvaluation().errorRateAndLength(new_genotype)
        if (new_fitness > fitness):
            # Jesli lepszy genotyp, podmien
            genotype.set_direct(new_genotype.direct())
        elif (random.uniform(0.0, 1.0) < self.bad_mutation_probability):
            # Jesli gorszy, podmien z szansa = bad_mutation_probability
            genotype.set_direct(new_genotype.direct())


# Znamy min i max fitness, mozna przeskalowac na sile mutacji (kazdy genotyp ma zapisane)
# Jesli min i max na wyspie sa takie same, bierzemy globalne max aby zwiekszyc sile mutacji 
# Zakladamy, ze minimalny fitness w populacji wynosi 0
# Skalujemy liniowo na sile mutacji z przedzialu (min_str;max_str)
def get_mutation_power(fitness, min_fitness, max_fitness, min_strength, max_strength):
    fitness = max(fitness, min_fitness)
    fitness = min(fitness, max_fitness)
    if (max_fitness == min_fitness):
        if (max_fitness == 0):
            return min_strength
        #TODO: zapytac Lukasza
        if CommaStatistics.max_fitness == 0 or CommaStatistics.max_fitness == min_fitness:
            bias = 0
        else:
            bias = float(fitness - min_fitness) / (CommaStatistics.max_fitness - min_fitness)
    else:
        bias = float(fitness - min_fitness) / (max_fitness - min_fitness)
    mutation_power = max_strength - math.floor(float(max_strength - min_strength) * bias)
    return int(mutation_power)


# Przemieszcza losowy marker w przestrzni pomiedzy sasiednimi markerami
def move_marker(genotype):
    # Losuj indeks segmentu
    index = random.randint(0, len(genotype.indirect()) - 2)

    left_segment_len = genotype.indirect()[index]
    right_segment_len = genotype.indirect()[index + 1]

    # Losuj o ile przesunac marker
    # Przesuwamy maksymalnie tak, zeby segmenty sasiadujace zmniejszyly sie do dlugosci 1
    change = random.randint(-left_segment_len + 1, right_segment_len - 1)

    # Utworz nowa reprezentacje z przesunietym markerem 
    new_indirect = genotype.indirect()[:]
    new_indirect[index] += change
    new_indirect[index + 1] -= change

    # Zmien genotyp na podstawie nowej reprezentacji indirect
    genotype.set_indirect(new_indirect)


# Przesuwa wszystkie markery o liczbe z przedzialu {-X, ..., X}, gdzie X to maksymalna sila mutacji
class MoveAllMarkersMutation(AbstractMutation):
    def __init__(self, max_strength):
        # 1.0 - probability, potrzebne tylko do konstruktora
        super(MoveAllMarkersMutation, self).__init__(GolombGenotype, 1.0)
        self.max_strength = max_strength

    def mutate(self, genotype):
        # old_genotype = genotype[:]
        move_all_markers(genotype, self.max_strength)

class CommaMoveAllMarkersMutation(AbstractMutation):
    def __init__(self, bad_mutation_probability=0.2, min_strength=1, max_strength=3):
        super(CommaMoveAllMarkersMutation, self).__init__(GolombGenotype, 1.0)
        self.min_strength = 1
        self.max_strength = 3
        self.bad_mutation_probability = bad_mutation_probability

    def mutate(self, genotype):
        # Fitness aktualnego rozwiazania
        fitness = GolombSimpleEvaluation().errorRateAndLength(genotype)
        # Kopia genow
        new_genotype = genotype.copy()

        mutation_power = get_mutation_power(fitness, genotype.min_fitness, genotype.max_fitness, self.min_strength,
                                           self.max_strength)

        move_all_markers(new_genotype, mutation_power)

        # Fitness nowego rozwiazania
        new_fitness = GolombSimpleEvaluation().errorRateAndLength(new_genotype)
        if (new_fitness > fitness):
            # Jesli lepszy genotyp, podmien
            genotype.set_direct(new_genotype.direct())
        elif (random.uniform(0.0, 1.0) < self.bad_mutation_probability):
            # Jesli gorszy, podmien z szansa = bad_mutation_probability
            genotype.set_direct(new_genotype.direct())

def move_all_markers(genotype, max_strength):
    new_indirect = genotype.indirect()[:]    
    for i in xrange(len(genotype.indirect())):
        new_indirect[i] = resize_segment(genotype.indirect()[i], max_strength)

    # Zmien orginalny genotyp na podstawie new_indirect
    genotype.set_indirect(new_indirect)

# Losuje jedno z postepowan i wykonuje dla wszystkich genow:
# 50%: Przesuwa wszystkie markery o liczbe z przedzialu {-X, ..., X}, gdzie X to maksymalna sila mutacji
# 50%: Shuffle - permutuje kolejnosc markerow. Every day I'm shuffling.
class MoveAllOrShuffleMutation(AbstractMutation):
    def __init__(self, max_strength):
        super(MoveAllOrShuffleMutation, self).__init__(GolombGenotype,
                                                       1.0)  # 1.0 - probability, potrzebne tylko do konstruktora
        self.max_strength = max_strength

    def mutate(self, genotype):
        # old_genotype = genotype[:]
        new_indirect = genotype.indirect()[:]

        move_or_shuffle(new_indirect, self.max_strength)

        genotype.set_indirect(new_indirect)

class CommaMoveAllOrShuffleMutation(AbstractMutation):
    def __init__(self, bad_mutation_probability=0.2, min_strength=1, max_strength=3):
        super(CommaMoveAllOrShuffleMutation, self).__init__(GolombGenotype,
                                                       1.0)  # 1.0 - probability, potrzebne tylko do konstruktora
        self.min_strength = 1
        self.max_strength = 3
        self.bad_mutation_probability = bad_mutation_probability

    def mutate(self, genotype):

        # Fitness aktualnego rozwiazania
        fitness = GolombSimpleEvaluation().errorRateAndLength(genotype)
        # Kopia genow
        new_genotype = genotype.copy()
        new_indirect = genotype.indirect()[:]

        mutation_power = get_mutation_power(fitness, genotype.min_fitness, genotype.max_fitness, self.min_strength,
                                           self.max_strength)
        move_or_shuffle(new_indirect, mutation_power)

        new_genotype.set_indirect(new_indirect)

        new_fitness = GolombSimpleEvaluation().errorRateAndLength(new_genotype)
        if (new_fitness > fitness):
            # Jesli lepszy genotyp, podmien
            genotype.set_direct(new_genotype.direct())
        elif (random.uniform(0.0, 1.0) < self.bad_mutation_probability):
            # Jesli gorszy, podmien z szansa = bad_mutation_probability
            genotype.set_direct(new_genotype.direct())

def move_or_shuffle(segments, max_strength):
    for i in xrange(len(segments)):
        if random.uniform(0.0, 1.0) < 0.5:
            segments[i] = resize_segment(segments[i], max_strength)
        else:
            random_pos = random.randint(0, len(segments) - 1)
            swap_segments(segments, i, random_pos)

def resize_segment(segment, max_strength):
    return max(1, segment + random.randint(-max_strength, max_strength))

def swap_segments(segments, index1, index2):
    tmp = segments[index1]
    segments[index1] = segments[index2]
    segments[index2] = tmp