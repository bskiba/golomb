import random

from pyage.core.emas import EmasAgent
from pyage.core.operator import Operator

try:
    from golomb.genotype import GolombGenotype
except:
    try:
        from genotype import GolombGenotype
    except:
        raise

try:
    from golomb.emas import FitnessSharingAgent
except:
    try:
        from emas import GolombAgent
    except:
        raise


# Tworzy genotyp - losowa stworzona liste jedynek i minus jedynek


class GolombInitializer(Operator):
    def __init__(self, order=3, size=100):
        super(GolombInitializer, self).__init__(GolombGenotype)
        self.size = size
        # rzad tworzonych linijek
        self.order = order

    def process(self, population):
        for i in xrange(self.size):
            # population.append(GolombGenotype([self.__randomize() for _ in xrange(self.order - 1)], direct=False))
            population.append(GolombGenotype(randomize_unique(self.order), direct=False))


# Losuje pojedynczy gen
def randomize(order):
    # TODO: rozwazyc jak zrobic losowanie, na razie arbitralnie ograniczone przez rzad^2 / 2
    # mozliwe rozwiazania to losowanie np calego ciagu roznych liczb
    return random.randint(1, min(order * 4, order ** 2 / 2))


# Losuje caly genotyp, tak aby posiadal unikalne dlugosci segmentow
def randomize_unique(order):
    segment_lengths = range(1, min(order * 4, order ** 2 / 2))
    genotype = []
    for _ in xrange(order - 1):
        random_index = random.randint(0, len(segment_lengths) - 1)
        genotype.append(segment_lengths[random_index])
        segment_lengths.remove(segment_lengths[random_index])
    # Every optimal golomb ruler should have a segment of width 1
    if not (1 in genotype):
        genotype[random.randint(0, len(genotype) - 1)] = 1
    return genotype


def golomb_emas_initializer(dims=2, energy=10, size=100):
    return __golomb_initializer(GolombAgent, dims, energy, size)


def __golomb_initializer(agent_type, dims, energy, size):
    agents = {}
    population = []
    init = GolombInitializer(dims, size)
    init.process(population)
    for genotype in population:
        agent = agent_type(genotype, energy)
        agents[agent.get_address()] = agent
    return agents
