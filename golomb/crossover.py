import random

from pyage.solutions.evolution.crossover import AbstractCrossover

try:
    from golomb.genotype import GolombGenotype
except:
    try:
        from genotype import GolombGenotype
    except:
        raise

# Krzyzowanie jednopunktowe dla Golomba, wykonywane na reprezentacji posredniej
class GolombSinglePointCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(GolombSinglePointCrossover, self).__init__(GolombGenotype, size)

    def cross(self, p1, p2):
        # TODO: umozliwia w tym momecie crossover powodujacy calkowite zastapienie jednym
        # z rodzicow
        crossingPoint = random.randint(0, len(p1.indirect()) - 1)
        # tworzymy z posredniej reprezentacji
        return GolombGenotype(p1.indirect()[:crossingPoint] + p2.indirect()[crossingPoint:],
                              max_fitness=p1.max_fitness, min_fitness=p1.min_fitness, direct=False)


# Krzyzowanie jednorodne dla Golomba, wykonywane na reprezentacji posredniej
# Kazdy gen jest losowany z prawdopodobienstwem 50% od jednego lub drugiego rodzica
# max_parent_influence to maksymalny wplyw jednego z rodzicow na genotyp - np 70% (wtedy jest
# pewnosc, ze co najmniej 30% genow kazdego z rodzicow znajdzie sie w genotype dziecka)
class GolombUniformCrossover(AbstractCrossover):
    def __init__(self, size=100, max_parent_influence=70):
        super(GolombUniformCrossover, self).__init__(GolombGenotype, size)
        self.max_parent_influence = max_parent_influence

    def cross(self, p1, p2):
        if (len(p1.indirect()) != len(p2.indirect())):
            print "Katastrofa"
        new_genotype = p1.indirect()[:]
        influence_of_p1 = 0
        influence_of_p2 = 0
        for i in xrange(len(p1.indirect()) - 1):
            # Jesli nastepny gen od p1 spowodowalby przekroczenie max_parent_influence, wez gen od p2
            if (100.0 * (influence_of_p1 + 1)) / len(p1.indirect()) > self.max_parent_influence:
                new_genotype[i] = p2.indirect()[i]
                influence_of_p2 += 1
            # Jesli losowanie wybralo rodzica p2 i nastepny gen od niego nie spowoduje przekroczenia max_parent_influence, wez gen od p2
            elif (random.uniform(0.0, 1.0) < 0.5 and (100.0 * (influence_of_p2 + 1)) / len(p1.indirect()) < self.max_parent_influence):
                new_genotype[i] = p2.indirect()[i]
                influence_of_p2 += 1
            # W pozostalych przypadkach wez gen od p1
            else:
                influence_of_p1 += 1
        return GolombGenotype(new_genotype, max_fitness=p1.max_fitness, min_fitness=p1.min_fitness, direct=False)
