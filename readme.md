Dobór parametrów do testu:
--------------------------
  Tworzymy plik (najlepiej w katalogu configs/), który jest prostym skryptem bashowym exportującym odpowiednie zmienne.

Uruchamianie testu:
-------------------
  ~$ ./run configs/<nazwa_pliku_z_konfiguracja>

  Pliki z logami zostaną utworzone w katalogu logs/, natomiast wyniki testów do katalogu results/. Nazwa pliku to data wystartowania testu.

Rysowanie wykresu:
------------------
  ~$ ./plot_last

  Narysuje wykres ostatnio policzonego testu, pod warunkiem, że się ukończył. Wykres zostanie wyświetlony na ekran.

  Plik graficzny z wykresem, konfiguracja użyta do symulacji oraz pliki z wynikami zostaną umieszczone w katalogu plots/plot_last_X.


Rysowanie średniego wykresu dla kilku przebiegów:
------------------
  ~$ ./plot_summary <scieżka_do_katalogu> [skip_calc]

  Narysuje uśredniony wykres dla wszystkich przebiegów, które zawarte są w podanym katalogu. Przebiegi muszą mieć taką samą liczbę kroków. Skrypt wyliczy średni wynik z plików fitness i stworzy nowe pliki zbiorcze dla najlepszego i średniego fitnessu, potem wyświetli na dwóch osobnych wykresach. Obliczenia mogą potrwać. Dopisanie "skip_calc" w drugim argumencie spowoduje, że obliczenia zostaną pominięte i wyświetlą się wykresy na podstawie wcześniej wyliczonych plików. Zalecany sposób użycia - skopiować kilka przebiegów dla takich samych parametrów do osobnego katalogu i odpalić skrypt. Skrypt nie ma zabezpieczeń przed głupotą użytkownika.
  
  Pliki graficzne z wykresami, konfiguracja użyta do symulacji oraz pliki z wynikami zostaną umieszczone w katalogu plots/plot_summary_X.