#!/bin/bash

if [[ ! -f "$1" ]]; then
    echo "Please provide path to config file in the first argument."
    exit 0
fi
if [[ -z "$2" ]]; then
	echo "Assuming default exec count 4" 	
	EXEC_COUNT=4
else
	EXEC_COUNT="$2"	
fi

./clean.sh

for i in $(seq $EXEC_COUNT)
do
	./run.sh $1 > /dev/null &
	sleep 2
done

wait
./plot_summary.sh results/
