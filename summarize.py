import sys
import os
import codecs

# This script looks for files with filenames ending with
# _best.txt
# _avg.txt
# calculates an average and deviation of values found in all such files,
# line by line, and outputs it into files
# best_summary.txt
# best_avg.txt
# All files in given dir must have the same number of lines and 2 columns per line,
# otherwise script behavior is undefined.

def main():
    if len(sys.argv) < 2:
        print "Usage: python summarize.py <dir_name>"
        return

    dir_path = sys.argv[1]
    dir_content = os.listdir(dir_path)

    best_files = [dir_path + '/' + file_path for file_path in dir_content if file_path.endswith('_best.txt')]
    avg_files = [dir_path + '/' + file_path for file_path in dir_content if file_path.endswith('_avg.txt')]

    calc_avg(best_files, dir_path + '/summary_best.txt')
    calc_avg(avg_files, dir_path + '/summary_avg.txt')


def calc_avg(files, output_file_path):
    with open(output_file_path, 'a') as output_file:
        num_files = len(files)
        # Check number of lines in files,
        # first line is always a header so don't count it
        num_lines = sum(1 for _ in open(files[0])) - 1
        # Init empty lists of zeros and summarize all columns
        # Calculate min, average and max values for each row
        sums = [0] * num_lines
        min = [999999999] * num_lines
        max = [-999999999] * num_lines
        for curr_file_path in files:
            with codecs.open(curr_file_path, 'r', 'utf-8') as curr_file:
                for line in curr_file:
                    if not line.startswith('#'):
                        [line_number, value] = line.split(' ')
                        line_number = int(line_number)
                        value = float(value)
                        sums[line_number] += value
                        if value < min[line_number]:
                            min[line_number] = value
                        if value > max[line_number]:
                            max[line_number] = value
        for i in xrange(0, num_lines):
            output_file.write('%d %f %f %f\n' % (i, (sums[i] + .0) / num_files, min[i], max[i]))


if __name__ == '__main__':
    main()